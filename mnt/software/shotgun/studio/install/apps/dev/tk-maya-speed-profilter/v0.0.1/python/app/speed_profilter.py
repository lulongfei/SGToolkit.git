# -*- coding: utf-8 -*-


import maya.cmds as cmds
from tank.platform.qt import QtCore, QtGui


class ListData:
    def __init__(self, data):
        self.__data = data

    def compile_data(self):
        datas = list()

        for key in self.__data.iterkeys():
            children = list()

            object = self.__data.get(key).get('object')
            attribuate = key.rsplit('_', 1)[1]
            speeds = self.__data.get(key).get('speeds')
            periods = self.__data.get(key).get('periods')

            if len(speeds) >= 2:
                for index in xrange(len(speeds)):
                    children = list()

                    object = object
                    attribuate = attribuate
                    speed = speeds[index]
                    period = periods[index]

                    children.extend(object)
                    children.append(attribuate)
                    children.append(speed)
                    children.append(period)

                    datas.append(children)

            else:

                children.extend(object)
                children.append(attribuate)
                children.extend(speeds)
                children.extend(periods)

                datas.append(children)

        return datas


class SpeedProfilter:
    def get_scene_animCurves(self):
        animCurves = [item for item in cmds.ls(typ='animCurve') if not cmds.referenceQuery(item, inr=1)]
        return animCurves

    def key_value_iteration(self, animCurve):
        time_change = cmds.keyframe(animCurve, q=1, tc=1)
        value_change = cmds.keyframe(animCurve, q=1, vc=1)

        keyNums = len(time_change)

        if len(time_change) >= 2:
            for index in xrange(1, keyNums):
                index_plus = index - 1
                speed_value = (value_change[index] - value_change[index_plus]) / (
                time_change[index] - time_change[index_plus])
                yield speed_value, [time_change[index - 1], time_change[index]]

    def get_valid_animCurve_period(self, animCurve, threshold):
        anim_speed_attr = dict()
        speed_values, speed_period = list(), list()

        connect_object = cmds.listConnections(animCurve, d=1) or list()

        for value, location in self.key_value_iteration(animCurve):
            if abs(value) >= threshold:
                speed_values.append(abs(float('%.3f' % value)))
                speed_period.append(location)

                anim_speed_attr['speeds'] = speed_values
                anim_speed_attr['periods'] = speed_period
                anim_speed_attr['object'] = connect_object

        return anim_speed_attr

    def get_all_valid_animCurve(self, threshold):
        up_threshold_anim = dict()
        anim_curves = self.get_scene_animCurves()
        for anim_curve in anim_curves:
            valid_speed_anim = self.get_valid_animCurve_period(anim_curve, threshold)
            if valid_speed_anim:
                up_threshold_anim[anim_curve] = valid_speed_anim

        return up_threshold_anim


class SpeedProfilterModel(QtCore.QAbstractTableModel):
    def __init__(self, datas=[]):
        QtCore.QAbstractTableModel.__init__(self)
        self.__header = ['Object', 'Attribute', 'Speed', 'Frame Range']
        self.__datas = datas

    def clear(self):
        self.reset()

    def rowCount(self, parent):
        return len(self.__datas)

    def columnCount(self, parent):
        return len(self.__header)

    def data(self, index, role=QtCore.Qt.DisplayRole):
        if role == QtCore.Qt.DisplayRole:
            row = index.row()
            column = index.column()
            if self.__datas:
                if isinstance(self.__datas[row][column], (list, tuple)):
                    return '--'.join(map(str, self.__datas[row][column]))
                return self.__datas[row][column]
        return

    def headerData(self, section, orientation, role):
        if role == QtCore.Qt.DisplayRole and orientation == QtCore.Qt.Horizontal:
            return self.__header[section]
        return

    def setData(self, index, value, role=QtCore.Qt.DisplayRole):
        print 'setData:', index.row(), index.column(), value

    def updateData(self, animData):
        self.__datas = animData
        self.reset()

    def insertRows(self, column, row, parent=QtCore.QModelIndex()):
        self.beginInsertRows(parent, column, row)
        self.endInsertRows()
        return True