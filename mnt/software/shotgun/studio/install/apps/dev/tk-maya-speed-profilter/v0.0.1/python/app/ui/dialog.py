# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialog.ui'
#
#      by: pyside-uic 0.2.13 running on PySide 1.1.0
#
# WARNING! All changes made in this file will be lost!

from tank.platform.qt import QtCore, QtGui

from ..speed_profilter import SpeedProfilter, SpeedProfilterModel, ListData


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        self.sp = SpeedProfilter()
        self.data_list = ListData

        self.__datas = []
        self.speed_profilter_model = SpeedProfilterModel(datas=self.__datas)

        vbox = QtGui.QVBoxLayout(Dialog)

        hbox = QtGui.QHBoxLayout(Dialog)
        vbox.addLayout(hbox)

        speed_label = QtGui.QLabel('Input Speed:')
        self.speedValueLe = QtGui.QLineEdit()
        self.search_animCurves_Btn = QtGui.QPushButton('Search')
        self.search_animCurves_Btn.setFixedWidth(60)
        self.select_objects_Btn = QtGui.QPushButton('Select Object')
        self.select_objects_Btn.setFixedWidth(100)
        hbox.addWidget(speed_label)
        hbox.addWidget(self.speedValueLe)
        hbox.addWidget(self.search_animCurves_Btn)
        hbox.addWidget(self.select_objects_Btn)

        self.table_view = QtGui.QTableView(Dialog)
        self.table_view.setSortingEnabled(True)
        self.table_view.setModel(self.speed_profilter_model)
        self.table_view.resizeRowsToContents()
        horizontal_header = self.table_view.horizontalHeader()
        horizontal_header.setStretchLastSection(True)

        vbox.addWidget(self.table_view)

        Dialog.setObjectName('Speed_Profilter_Win')
        Dialog.setWindowTitle('Speed Profilter Win')
        Dialog.resize(431, 392)
        # Dialog.setMinimumSize(432, 420)
        # self.setWindowFlags(QtCore.Qt.Window)

        # self.search_animCurves_Btn.clicked.connect(self.search_animCurves_Btn_command)
        # self.select_objects_Btn.clicked.connect(self.select_objects_Btn_command)

        # Dialog.setObjectName("Dialog")
        # Dialog.resize(431, 392)
        # self.horizontalLayout = QtGui.QHBoxLayout(Dialog)
        # self.horizontalLayout.setObjectName("horizontalLayout")

        ########
        # self.table_widget = QtGui.QTableWidget(Dialog)
        # self.header = ['Object', 'Attribute', 'Speed', 'Frame Range']
        # self.table_widget.setColumnCount(4)
        # self.table_widget.setHorizontalHeaderLabels(self.header)
        # self.table_widget.resize(431, 200)
        # self.horizontalLayout.addWidget(self.table_widget)
        ########

        # self.logo_example = QtGui.QLabel(Dialog)
        # self.logo_example.setText("")
        # self.logo_example.setPixmap(QtGui.QPixmap(":/res/sg_logo.png"))
        # self.logo_example.setObjectName("logo_example")
        # self.horizontalLayout.addWidget(self.logo_example)
        # self.context = QtGui.QLabel(Dialog)
        # sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        # sizePolicy.setHorizontalStretch(0)
        # sizePolicy.setVerticalStretch(0)
        # sizePolicy.setHeightForWidth(self.context.sizePolicy().hasHeightForWidth())
        # self.context.setSizePolicy(sizePolicy)
        # self.context.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        # self.context.setObjectName("context")
        # self.horizontalLayout.addWidget(self.context)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "The Current Sgtk Environment", None, QtGui.QApplication.UnicodeUTF8))
        # self.context.setText(QtGui.QApplication.translate("Dialog", "Your Current Context: ", None, QtGui.QApplication.UnicodeUTF8))

from . import resources_rc
