# Copyright (c) 2013 Shotgun Software Inc.
# 
# CONFIDENTIAL AND PROPRIETARY
# 
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit 
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your 
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights 
# not expressly granted therein are reserved by Shotgun Software Inc.

import sgtk
import os
import sys
import threading

# by importing QT from sgtk rather than directly, we ensure that
# the code will be compatible with both PySide and PyQt.
from sgtk.platform.qt import QtCore, QtGui
from .ui.dialog import Ui_Dialog

def show_dialog(app_instance):
    """
    Shows the main dialog window.
    """
    # in order to handle UIs seamlessly, each toolkit engine has methods for launching
    # different types of windows. By using these methods, your windows will be correctly
    # decorated and handled in a consistent fashion by the system. 
    
    # we pass the dialog class to this method and leave the actual construction
    # to be carried out by toolkit.
    # app_instance.engine.show_dialog("Starter Template App...", app_instance, AppDialog)
    app_instance.engine.show_dialog("Speed Profilter", app_instance, AppDialog)
    


class AppDialog(QtGui.QWidget):
    """
    Main application dialog window
    """
    
    def __init__(self):
        """
        Constructor
        """
        # first, call the base class and let it do its thing.
        QtGui.QWidget.__init__(self)
        
        # now load in the UI that was created in the UI designer
        self.ui = Ui_Dialog() 
        self.ui.setupUi(self)

        self.ui.search_animCurves_Btn.clicked.connect(self.search_animCurves_Btn_command)
        self.ui.select_objects_Btn.clicked.connect(self.select_objects_Btn_command)

        # most of the useful accessors are available through the Application class instance
        # it is often handy to keep a reference to this. You can get it via the following method:
        self._app = sgtk.platform.current_bundle()
        
        # via the self._app handle we can for example access:
        # - The engine, via self._app.engine
        # - A Shotgun API instance, via self._app.shotgun
        # - A tk API instance, via self._app.tk 
        
        # lastly, set up our very basic UI
        # self.ui.context.setText("Current Context: %s" % self._app.context)

    def get_speed_value(self):
        value = float(self.ui.speedValueLe.text()) if self.ui.speedValueLe.text() else 0
        return value

    def get_table_data(self):
        speed_value = self.get_speed_value()
        if speed_value:
            datas = self.ui.sp.get_all_valid_animCurve(speed_value)
            return datas

        return

    def search_animCurves_Btn_command(self, *args):
        self.__datas = []
        datas = self.get_table_data()

        if datas:
            _datas = self.ui.data_list(datas).compile_data()
            self.__datas.extend(_datas)

            self.ui.speed_profilter_model.updateData(self.__datas)
        else:
            self.ui.speed_profilter_model.updateData(self.__datas)

    def select_objects_Btn_command(self, *args):
        import maya.cmds as cmds
        datas = self.get_table_data()
        objects = list(set([datas.get(item).get('object')[0] for item in datas.iterkeys()]))
        cmds.select(objects, r=1)
