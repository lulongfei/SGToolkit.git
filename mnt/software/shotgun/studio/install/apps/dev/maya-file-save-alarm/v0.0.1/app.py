"""
Copyright (c) 2015 Original Force, Inc
----------------------------------------------------

Save file alarm in Maya.

"""
from tank.platform import Application
from tank.platform.qt import QtCore, QtGui
import maya.cmds as cmds

import sys
import os
from datetime import datetime


class SaveFileAlarm(Application):
    def init_app(self):
        of_maya_file_save_alarm = self.import_module("of_maya_file_save_alarm")
        self.app_handler = of_maya_file_save_alarm.AppHandler(self)

        self.engine.register_command("Save File Alarm", 
                                     self.app_handler.show_setting_dialog, 
                                     {"tooltip": "Fixed timer to remind user to save file."})
        self._alarmClock = QtCore.QTimer()
        self._alarmClock.timeout.connect(self.onTime)
        self._interval_value = of_maya_file_save_alarm.FileSaveSetting.getIntervalValue()
        
        self.alarmStatus = of_maya_file_save_alarm.FileSaveSetting.ALARM_STATUS_ON  # The status of the alarm is always on 
                                                                                    # except user stop the alarm manually
        
        if self.context and self.context.user.get('id') == 137:
            return

        if self._interval_value:
            self.log_debug('self._interval_value: %s'%self._interval_value)
            self.log_debug('time init: %s'%datetime.now())
            self.startAlarm()

    def onTime(self):
        self.log_debug('on time:%s'%datetime.now())
        self.stopAlarm()
        if not cmds.file(q=1, sceneName=True):
            QtGui.QMessageBox.warning(None, 'Warning', 'Please name the scene!', QtGui.QMessageBox.Ok)
            self.startAlarm()
            return
        
        self.app_handler.show_warning_dialog({'startAlarm':self.startAlarm})
        return
    
    @property
    def intervalValue(self):
        return self._interval_value
    
    @intervalValue.setter
    def intervalValue(self, value):
        self._interval_value = value
        self.log_debug('reset interval: %s'%value)
        self.stopAlarm()
        if value:
            self.startAlarm()
            self.log_debug('restart: %s'%datetime.now())
    
    def startAlarm(self):
        interval=self._interval_value * 60 * 1000
        if self._alarmClock:
            if self._alarmClock.isActive():  # if current timer is active, restart
                self.stopAlarm()
            self._alarmClock.start(interval)
            self.log_debug('start...')

    def stopAlarm(self):
        if self._alarmClock:
            self._alarmClock.stop()
            self.log_debug('stop')

    def destroy_app(self):
        self.stopAlarm()
        self.log_debug("Destroying of-maya-file-save-alarm")

