# -*- coding: utf-8 -*-
"""
# Copyright (c) 2015 Original Force, Inc
----------------------------------------------------
"""

from tank.platform.qt import QtCore, QtGui

from .customSlider import SliderCustom
from .thumbnail_label import ThumbnailLabel
from .. import resources_rc


class SaveFileSettingWindow(object):
    def setup_ui(self, Canvas):
        formLayout = QtGui.QFormLayout(Canvas)

        self.slider_interval = SliderCustom(Canvas)
        self.slider_interval.setFixedHeight(45)
        formLayout.addRow("Interval:", self.slider_interval)
        
        hBoxLayout = QtGui.QHBoxLayout()
        self.bt_stop = QtGui.QPushButton("Stop")
        self.bt_save = QtGui.QPushButton("Save|Start")

        self.lb_alarm = ThumbnailLabel()
        self.lb_alarm.setPixmap(QtGui.QPixmap(":/icons/clock32.png"))
        self.lb_interval = QtGui.QLabel()
        
        hBoxLayout.addWidget(self.lb_alarm)
        hBoxLayout.addWidget(self.lb_interval)
        hBoxLayout.addStretch(1)
        hBoxLayout.addWidget(self.bt_stop)
        hBoxLayout.addWidget(self.bt_save)
        formLayout.addRow("", hBoxLayout)
