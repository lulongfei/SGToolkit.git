# -*- coding: utf-8 -*-

from .fileSaveSetting import FileSaveSetting
from .saveAndBakFile import FileBakWarningWindow

class AppHandler(object):
    def __init__(self, app):
        self._app = app

    def show_setting_dialog(self):
        self._app.engine.show_modal("Save File Setting", self._app, FileSaveSetting, app=self._app)

    def show_warning_dialog(self, callbacks):
    	self._app.engine.show_modal("Save File", self._app, FileBakWarningWindow, app=self._app, callbacks=callbacks)
