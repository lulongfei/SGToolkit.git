import os
import shutil
from datetime import datetime
import maya.cmds as cmds
import functools

from tank.platform.qt import QtCore, QtGui
from .ui import FileSaveWindow

class FileBakWarningWindow(QtGui.QWidget):
    def __init__(self, app, callbacks, parent=None):
        super(FileBakWarningWindow, self).__init__(parent)
        self.resize(200, 150)
        self._app = app      
        self._callbacks = callbacks
        
        self.snap_tmpl = self._app.get_template("template_snapshot")
        self._fields = self._app.context.as_template_fields(self.snap_tmpl)
        self.ui = FileSaveWindow()
        self.ui.setup_ui(self)

        self.initData()

    def initData(self):
        self.ui.bt_save.clicked.connect(self.saveAndBakFile)
        
        snapshotDir = self.snap_tmpl.parent.apply_fields(self._fields)
        self.ui.lineEdit_dir.setText(snapshotDir)
        return
    
    def saveAndBakFile(self):
        cmds.file(save=True, force=True)
        if self.ui.checkBox_bak.checkState() == QtCore.Qt.Checked:  # bac file
            currentFile = cmds.file(q=1, sceneName=True)
            fileName, fileExt = os.path.splitext(os.path.basename(currentFile))
            timestamp = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
            fileNameBak = '%s_autoBak_%s%s'%(fileName, timestamp, fileExt)
            
            bakDir = self.ui.lineEdit_dir.text()
            fileNameBakPath = '%s/%s'%(bakDir, fileNameBak)

            if not os.path.exists(bakDir):
                os.makedirs(bakDir, 0777)
            shutil.copy(currentFile, fileNameBakPath)
            self._app.log_info("Bak: Copying %s --> %s" % (currentFile, fileNameBakPath))

        self.close()

    def closeEvent(self, event):
        if 'startAlarm' in self._callbacks:
            self._callbacks['startAlarm']()
        event.accept()
