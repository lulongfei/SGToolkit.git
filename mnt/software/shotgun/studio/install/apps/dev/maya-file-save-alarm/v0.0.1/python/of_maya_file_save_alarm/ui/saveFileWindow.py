# -*- coding: utf-8 -*-
"""
# Copyright (c) 2015 Original Force, Inc
----------------------------------------------------
"""

from tank.platform.qt import QtCore, QtGui

class FileSaveWindow(object):
    def setup_ui(self, Canvas):
        vBoxLayout = QtGui.QVBoxLayout(Canvas)

        self.optionsGroupBox = QtGui.QGroupBox("Bak")
        formLayout = QtGui.QFormLayout(Canvas)
        
        self.lineEdit_dir = QtGui.QLineEdit()
        self.lineEdit_dir.setReadOnly(True)
        formLayout.addRow("Dir:", self.lineEdit_dir)

        self.lb_fileName_format = QtGui.QLabel("{Scene}_autoBak_{Timestamp}")
        formLayout.addRow("Name Format:", self.lb_fileName_format)
        
        self.checkBox_bak = QtGui.QCheckBox()
        formLayout.addRow("Bak:", self.checkBox_bak)

        self.optionsGroupBox.setLayout(formLayout)
        
        hBoxLayout = QtGui.QHBoxLayout()
        self.bt_save = QtGui.QPushButton("Save")
        hBoxLayout.addStretch(1)
        hBoxLayout.addWidget(self.bt_save)

        vBoxLayout.addWidget(self.optionsGroupBox)
        vBoxLayout.addLayout(hBoxLayout)