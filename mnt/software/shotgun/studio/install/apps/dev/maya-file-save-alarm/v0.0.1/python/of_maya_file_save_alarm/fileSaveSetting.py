import maya.cmds as cmds

from tank.platform.qt import QtCore, QtGui

from .ui import SaveFileSettingWindow

INTERVAL_VARIABLE = "saveFileInterval"

class FileSaveSetting(QtGui.QWidget):
    SAVE_INTERVAL_DEFAULT = 30 # default interval: 30 mins
    SAVE_INTERVAL_MIN = 10 # stop alarm
    SAVE_INTERVAL_MAX = 60*2 # interval max value: 8 hours
    INTERVAL_UNIT = 10

    ALARM_STATUS_STOP = 0
    ALARM_STATUS_ON = 1
    def __init__(self, app, parent=None):
        super(FileSaveSetting, self).__init__(parent)
        self.resize(200, 90)
        self._app = app
        self.ui = SaveFileSettingWindow()
        self.ui.setup_ui(self)

        self.init_data()

    def init_data(self):
        self.ui.slider_interval.setOrientation(QtCore.Qt.Horizontal)
        self.ui.slider_interval.setRange(self.SAVE_INTERVAL_MIN, self.SAVE_INTERVAL_MAX)
        self.ui.slider_interval.setTickInterval(self.INTERVAL_UNIT*2)
        self.ui.slider_interval.setSingleStep(self.INTERVAL_UNIT)
        self.ui.slider_interval.setTickPosition(QtGui.QSlider.TicksAbove)

        self._interval_saved = self.getIntervalValue()
        self.ui.slider_interval.setValue(self._interval_saved)
        self.ui.lb_interval.setText("%s mins"%self._interval_saved)
        if self._app.alarmStatus == self.ALARM_STATUS_STOP:
            self.ui.lb_alarm.setVisible(False)
            self.ui.lb_interval.setVisible(False)
            self.ui.bt_stop.setEnabled(False)

        self.ui.slider_interval.valueChanged.connect(self.onIntervalChanged)
        self.ui.bt_save.clicked.connect(self.saveInterval)
        self.ui.bt_stop.clicked.connect(self.stopAlarmClock)
        return

    def onIntervalChanged(self, value):
        self.ui.lb_interval.setText("%s mins"%value)
        return

    def stopAlarmClock(self):
        self._app.stopAlarm()
        self.setAlarmStatus(self.ALARM_STATUS_STOP)
        return

    def saveInterval(self):
        self._interval_saved = self.ui.slider_interval.value()
        self.saveIntervalValue(self._interval_saved)
        self.updateTimerInterval(self._interval_saved)
        self.close()

    @classmethod
    def getIntervalValue(cls):
        if cmds.optionVar( ex = INTERVAL_VARIABLE ):
            interval = cmds.optionVar( q = INTERVAL_VARIABLE )
            if interval and interval>= cls.SAVE_INTERVAL_MIN and interval <= cls.SAVE_INTERVAL_MAX:
                return interval
            else:
                return cls.SAVE_INTERVAL_MIN
        else:
            return cls.SAVE_INTERVAL_DEFAULT

    @classmethod
    def saveIntervalValue(cls, value):
        cmds.optionVar( iv = (INTERVAL_VARIABLE, value) )
   
    def updateTimerInterval(self, value):
        self.setAlarmStatus(self.ALARM_STATUS_ON)
        self.ui.lb_interval.setText("%s mins"%self._interval_saved)
        self._app.intervalValue = value

    def setAlarmStatus(self, alarmStatus):
        self.ui.lb_alarm.setVisible(alarmStatus == self.ALARM_STATUS_ON)
        self.ui.lb_interval.setVisible(alarmStatus == self.ALARM_STATUS_ON)
        self.ui.bt_stop.setEnabled(alarmStatus == self.ALARM_STATUS_ON)
        self._app.alarmStatus = alarmStatus
