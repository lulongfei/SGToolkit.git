# -*- coding: utf-8 -*-
from __future__ import division
from tank.platform.qt import QtGui, QtCore


class SliderCustom(QtGui.QSlider):
    def __init__(self, parent=None):
        super(SliderCustom, self).__init__(parent)
        self.setMinimumHeight(45)

    def paintEvent(self, e):
        qp = QtGui.QPainter()
        qp.begin(self)
        self.drawWidget(qp)
        qp.end()
        super(SliderCustom, self).paintEvent(e)

    def drawWidget(self, qp):
        font = QtGui.QFont('Serif', 7, QtGui.QFont.Light)
        qp.setFont(font)

        size = self.size()
        w = size.width()
        h = size.height()
  
        # point = self.mapToGlobal(QtCore.QPoint(self.x(), self.y()))
        sliderBlockWidthHalf = 5
        widthUnit = w/(self.maximum()-self.minimum())

        # draw number
        steps = range(self.minimum(), self.maximum()+1, self.tickInterval())
        if self.maximum() not in steps:
            steps.append(self.maximum())
        for i in steps: 
            metrics = qp.fontMetrics()
            fw = metrics.width(str(i))
            fh = metrics.height()
            x = 0
            x_tick = 0
            if i == self.minimum():
                x = 0
                x_tick = 0
            elif i == self.maximum():
                x = w - fw
                x_tick = w
            else:
                x_tick = (i-self.minimum())*widthUnit 
                x = (i-self.minimum())*widthUnit - max(fw/2, sliderBlockWidthHalf)
            # qp.drawLine(x_tick, h/2, x_tick, h)
            qp.drawText(x, self.y()+ h-fh, str(i)) 