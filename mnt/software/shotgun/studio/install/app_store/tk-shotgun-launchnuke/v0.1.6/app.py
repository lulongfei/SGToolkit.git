"""
Copyright (c) 2012 Shotgun Software, Inc
----------------------------------------------------

App that launches Nuke from inside of Shotgun.

"""
import os
import re
import sys
import tank

class LaunchNuke(tank.platform.Application):

    def init_app(self):
        entity_types = self.get_setting("entity_types")
        deny_permissions = self.get_setting("deny_permissions")
        deny_platforms = self.get_setting("deny_platforms")
        menu_name = self.get_setting("menu_name")

        p = {
            "title": menu_name,
            "entity_types": entity_types,
            "deny_permissions": deny_permissions,
            "deny_platforms": deny_platforms,
            "supports_multiple_selection": False
        }

        # the command name mustn't contain spaces and funny chars, so sanitize it before
        # passing it in...
        sanitized_menu_name = re.sub(r"\W+", "", menu_name)

        self.engine.register_command(sanitized_menu_name , self.launch_nuke, p)

    def _execute_app(self, file_to_open=None):
        """
        Executes the app in a platform specific fashion
        """

        # add our startup path to the nuke init path
        startup_path = os.path.abspath(os.path.join( os.path.dirname(__file__), "startup"))
        tank.util.append_path_to_env_var("NUKE_PATH", startup_path)

        self.execute_hook("hook_before_app_launch")

        # get the setting
        system = sys.platform
        try:
            path_key = r"%s_path"
            args_key = r"%s_args"
            system_name = {"linux2": "linux", "darwin": "mac", "win32": "windows"}[system]
            app_path = self.get_setting(path_key % system_name, "")
            app_args = self.get_setting(args_key % system_name, "")
            if not app_path: raise KeyError()
        except KeyError:
            raise Exception("Platform '%s' is not supported." % system)

        # run the app

        if file_to_open is None:
            # open empty nuke
            if system == "linux2":
                cmd = '%s %s &' % (app_path, app_args)
            elif system == "darwin":
                cmd = 'open -n "%s"' % (app_path)
                if app_args:
                    cmd += ' --args "%s"' % (app_args)
            elif system == "win32":
                cmd = 'start /B "Nuke" "%s" %s' % (app_path, app_args)
            else:
                raise Exception("Platform '%s' is not supported." % system)

        else:
            # open file after startup
            if system == "linux2":
                cmd = '%s %s "%s" &' % (app_path, app_args, file_to_open)
            elif system == "darwin":
                args = file_to_open
                if app_args:
                    args += " " + app_args
                cmd = 'open -n "%s" --args "%s"' % (app_path, args)
            elif system == "win32":
                cmd = 'start /B "Nuke" "%s" "%s" %s' % (app_path, file_to_open, app_args)
            else:
                raise Exception("Platform '%s' is not supported." % system)

        self.log_debug("Executing launch command '%s'" % cmd)
        exit_code = os.system(cmd)
        if exit_code != 0:
            self.log_error("Failed to launch Nuke! This is most likely because the path "
                          "to the nuke executable is not set to a correct value. The "
                          "current value is '%s' - please double check that this path "
                          "is valid and update as needed in this app's configuration. "
                          "If you have any questions, don't hesitate to contact support "
                          "on tanksupport@shotgunsoftware.com." % app_path)
        return cmd
    
    def _register_event_log(self, ctx, command_executed, additional_meta):
        """
        Writes an event log entry to the shotgun event log, informing
        about the app launch
        """        
        meta = {}
        meta["engine"] = "%s %s" % (self.engine.name, self.engine.version) 
        meta["app"] = "%s %s" % (self.name, self.version) 
        meta["command"] = command_executed
        meta["platform"] = sys.platform
        if ctx.task:
            meta["task"] = ctx.task["id"]
        meta.update(additional_meta)
        desc =  "%s %s: Launched Nuke" % (self.name, self.version)
        tank.util.create_event_log_entry(self.tank, ctx, "Tank_Nuke_Startup", desc, meta)
    

    def launch_from_path(self, path):
        # Store data needed for bootstrapping Tank in env vars. Used in startup/menu.py
        os.environ["TANK_NUKE_ENGINE"] = self.get_setting("engine")
        os.environ["TANK_NUKE_PROJECT_ROOT"] = self.tank.project_path
        os.environ["TANK_NUKE_FILE_TO_OPEN"] = path

        # now launch nuke!
        cmd = self._execute_app(path)
        
        # write an event log entry
        ctx = self.tank.context_from_path(path)  
        self._register_event_log(ctx, cmd, {"path": path})  
        

    def launch_from_entity(self, entity_type, entity_id):
        # Store data needed for bootstrapping Tank in env vars. Used in startup/menu.py
        os.environ["TANK_NUKE_ENGINE"] = self.get_setting("engine")
        os.environ["TANK_NUKE_PROJECT_ROOT"] = self.tank.project_path
        os.environ["TANK_NUKE_ENTITY_TYPE"] = entity_type
        os.environ["TANK_NUKE_ENTITY_ID"] = str(entity_id)

        # now launch nuke!
        cmd = self._execute_app()
        
        # write an event log entry
        ctx = self.tank.context_from_entity(entity_type, entity_id)
        self._register_event_log(ctx, cmd, {})
        

    def launch_nuke(self, entity_type, entity_ids):

        if len(entity_ids) != 1:
            raise Exception("LaunchNuke only accepts a single item in entity_ids.")

        entity_id = entity_ids[0]

        # Try to create path for the context.
        try:
            self.tank.create_filesystem_structure(entity_type, entity_id, engine="tk-nuke")
        except tank.TankError, e:
            raise Exception("Could not create folders on disk. Error reported: %s" % e)            

        # kick off the app
        self.launch_from_entity(entity_type, entity_id)


