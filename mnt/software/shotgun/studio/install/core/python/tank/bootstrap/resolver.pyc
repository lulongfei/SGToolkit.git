ó
ud1Xc           @   sï   d  Z  d d l Z d d l Z d d l Z d d l m Z m Z m Z d d l m	 Z	 d d l
 m Z d d l m Z d d	 l m Z d d
 l m Z d d l m Z d d l m Z d d l m Z e j e  Z d e f d     YZ d S(   sZ   
Resolver module. This module provides a way to resolve a pipeline configuration
on disk.
iÿÿÿÿNi   (   t
   Descriptort   create_descriptort   descriptor_uri_to_dicti   (   t   TankBootstrapError(   t   BakedConfiguration(   t   CachedConfiguration(   t
   filesystem(   t   ShotgunPath(   t   LocalFileStorageManager(   t
   LogManager(   t	   constantst   ConfigurationResolverc           B   sA   e  Z d  Z d d d  Z d   Z d   Z d   Z d   Z RS(   sv   
    A class that contains the business logic for returning a configuration
    object given a set of parameters.
    c         C   sW   | |  _  |  j  r) i d d 6|  j  d 6n d |  _ | |  _ | |  _ | pM g  |  _ d S(   s  
        Constructor

        :param plugin_id: The plugin id of the system that is being bootstrapped.
        :param engine_name: Name of the engine that is about to be launched.
        :param project_id: Project id to create a config object for, None for the site config.
        :param bundle_cache_fallback_paths: Optional list of additional paths where apps are cached.
        t   Projectt   typet   idN(   t   _project_idt   Nonet   _proj_entity_dictt
   _plugin_idt   _engine_namet   _bundle_cache_fallback_paths(   t   selft	   plugin_idt   engine_namet
   project_idt   bundle_cache_fallback_paths(    (    sM   D:\mnt\software\shotgun\studio\install\core\python\tank\bootstrap\resolver.pyt   __init__'   s
    	)		c         C   s   d |  j  |  j |  j f S(   Ns/   <Resolver: proj id %s, engine %s, plugin id %s>(   R   R   R   (   R   (    (    sM   D:\mnt\software\shotgun\studio\install\core\python\tank\bootstrap\resolver.pyt   __repr__<   s    c         C   s'  t  j d |  | f  | d k r2 t d   n  t | t  rP t |  } n  | d t j k r+d } t  j d |  xm |  j	 D]b } t
 j j | t j | d | d  } t
 j j |  r t  j d |  t j |  } Pq q W| d k r	t d |   n  t | | |  j |  j d |  j	  Sd | k rMt  j d	  t } n t  j d
  t } t | t j | d |  j	 d | } t  j d |  t j | j |  j |  j d t j  } t
 j j | d  }	 t j |	  t  j d |	  t j |	  }
 t |
 | | |  j |  j d |  j	  Sd S(   sê   
        Return a configuration object given a config descriptor

        :param config_descriptor: descriptor dict or string
        :param sg_connection: Shotgun API instance
        :return: :class:`Configuration` instance
        s,   %s resolving configuration for descriptor %ssF   No config descriptor specified - Cannot create a configuration object.R   s   Searching for baked config %st   namet   versions   Located baked config in %ss   Cannot locate %s!s^   Base configuration has a version token defined. Will use this fixed version for the bootstrap.s|   Base configuration descriptor does not have a version token defined. Will attempt to determine the latest version available.t   fallback_rootst   resolve_latests   Configuration resolved to %r.t   cfgs"   Configuration root resolved to %s.N(    t   logt   debugR   R   t
   isinstancet   strR   R
   t   BAKED_DESCRIPTOR_TYPER   t   ost   patht   joint   BAKED_DESCRIPTOR_FOLDER_NAMEt   existsR   t   from_current_os_pathR   R   R   t   Falset   TrueR   R    t   CONFIGR   t   get_configuration_roott   base_urlt   CACHER   t   ensure_folder_existsR   (   R   t   config_descriptort   sg_connectiont   baked_config_roott	   root_patht   baked_config_pathR   t   cfg_descriptort
   cache_roott   config_cache_roott   config_root(    (    sM   D:\mnt\software\shotgun\studio\install\core\python\tank\bootstrap\resolver.pyt   resolve_configurationC   st    	

			c      
   C   s,  t  j d |  | f  d d d d d d d d	 d
 d g
 } d+ } | d+ k rUt  j d  t  j d  | j d i d d 6i d d 6d d |  j g d d d+ g g d 6i d d 6d d t j g d d | g g d 6g d 6g | d i d d 6d d 6g } t  j d t j |   d+ } d+ }	 d+ }
 d+ } xß | D]× } |  j	 | j
 d   sk|  j	 | j
 d   r5| d |  j k rÅ| d t j k r«t  j d |  | } q	| }	 t  j d |  q| d t j k rò| }
 t  j d |  q| } t  j d |  q5q5W|	 r|	 } q4| r.| } q4| r=| } q4|
 rL|
 } q4d+ } nß t  j d  |  t  j d!  | j d d d |  j g d d | g g | d i d d 6d d 6g } t  j d t j |   x] | D]U } |  j	 | j
 d   s|  j	 | j
 d   rÛ| r't  j d"  n  | } qÛqÛW| } | d+ k rVt  j d#  nµ t  j d$ t j |   t j |  } | j r¯t  j d%  i d& d' 6| j d& 6} n\ | j
 d  rÝt  j d(  | j
 d  } n. | j
 d
  rt  j d)  | j
 d
  } n  t  j d* |  |  j | |  S(,   s\  
        Return a configuration object by requesting a pipeline configuration
        in Shotgun. If no suitable configuration is found, return a configuration
        for the given fallback config.

        :param pipeline_config_name: Name of configuration branch (e.g Primary).
                                     if None, the method will automatically attempt
                                     to resolve the right configuration based on the
                                     current user and the users field on the pipeline
                                     configuration.
        :param fallback_config_descriptor: descriptor dict or string for fallback config.
        :param sg_connection: Shotgun API instance
        :param current_login: The login of the currently logged in user.

        :return: :class:`Configuration` instance
        sA   %s resolving configuration from Shotgun Pipeline Configuration %st   codet   projectt   userst
   plugin_idst   sg_plugin_idst   windows_patht
   linux_patht   mac_patht   sg_descriptort
   descriptors5   Will auto-detect which pipeline configuration to use.s2   Requesting pipeline configurations from Shotgun...t   PipelineConfigurationt   allt   filter_operatort   anyt   ist   filterss   users.HumanUser.logint   containst   ordert
   updated_att
   field_namet   asct	   directions4   The following pipeline configurations were found: %ss   Primary match: %ss   Per-user match: %ss    Found primary fallback match: %ss!   Found per-user fallback match: %ss$   Will use pipeline configuration '%s's6   Requesting pipeline configuration data from Shotgun...sO   More than one pipeline config detected. Will use the most recently updated one.s:   No pipeline configuration found. Using fallback descriptors5   The following pipeline configuration will be used: %ssC   Descriptor will be based off the path in the pipeline configurationR'   R   sO   Descriptor will be based off the descriptor field in the pipeline configurationsR   Descriptor will be based off the sg_descriptor field in the pipeline configurations,   The descriptor representing the config is %sN(   R!   R"   R   t   findR   R
   t   PRIMARY_PIPELINE_CONFIG_NAMEt   pprintt   pformatt   _match_plugin_idt   gett   warningR   t   from_shotgun_dictt
   current_osR<   (   R   t   pipeline_config_namet   fallback_config_descriptorR4   t   current_logint   fieldst   pipeline_configt   pipeline_configst   primary_configt   user_configt   primary_config_fallbackt   user_config_fallbackt   pcRF   R'   (    (    sM   D:\mnt\software\shotgun\studio\install\core\python\tank\bootstrap\resolver.pyt   resolve_shotgun_configuration·   s¶    	0						0

	c         C   s   | d k r t Sg  | j d  D] } | j   ^ q  } xA | D]9 } t j |  j |  r? t j d |  j | f  t Sq? Wt S(   sa  
        Given a plugin id pattern, determine if the current
        plugin id matches.

        Patterns can be comma separated and glob style patterns.
        Examples:

            - basic.nuke, basic.maya
            - basic.*, rv_review

        :param value: pattern string to check or None
        :return: True if matching false if not
        t   ,s'   Our plugin id '%s' matches pattern '%s'N(	   R   R,   t   splitt   stript   fnmatchR   R!   R"   R-   (   R   t   valuet   chunkt   patternst   pattern(    (    sM   D:\mnt\software\shotgun\studio\install\core\python\tank\bootstrap\resolver.pyRW     s    (N(	   t   __name__t
   __module__t   __doc__R   R   R   R<   Rg   RW   (    (    (    sM   D:\mnt\software\shotgun\studio\install\core\python\tank\bootstrap\resolver.pyR   !   s   		t	×(   Rr   R&   Rk   RU   RF   R    R   R   t   errorsR   t   baked_configurationR   t   cached_configurationR   t   utilR   R   R   t    R	   R
   t
   get_loggerRp   R!   t   objectR   (    (    (    sM   D:\mnt\software\shotgun\studio\install\core\python\tank\bootstrap\resolver.pyt   <module>   s   