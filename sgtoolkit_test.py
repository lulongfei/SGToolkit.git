path = r'D:\mnt\software\shotgun\studio\install\core\python'
import sys

if path not in sys.path:
    sys.path.insert(0, path)

import tank

project_root = r'D:\mnt\software\shotgun\sgtktest'

tk = tank.Tank(project_root)
# print tk.version

tankTaskID = 5705

context = tk.context_from_entity('Task', int(tankTaskID))
tk.create_filesystem_structure('Task', int(tankTaskID))

engine = tank.platform.current_engine()
if engine :
    engine.destroy()
else :
    pass
    
tankEngine = 'tk-maya'
engine = tank.platform.start_engine(tankEngine, context.tank, context)
engine.version